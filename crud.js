const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');
var db = require('./db.js');

const storage = multer.diskStorage({
  destination: './src/image/',
  filename: (req, file, cb) => {
    return cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`)
  }
});

const upload = multer({
  storage: storage
});

const eventData = [];

// POST api
router.post("/api/add_event", upload.single('image'), (req, res) => {
  if (!req.file) {
    return res.status(400).json({ error: 'No file uploaded' });
  }

  const { eevent, etime, elocation } = req.body;
  const imageFilename = req.file.filename; // Get the uploaded image filename

  const sql = "INSERT INTO crud (eevent, etime, elocation, image) VALUES (?, ?, ?, ?)";
  const values = [eevent, etime, elocation, imageFilename];

  db.query(sql, values, (err, result) => {
    if (err) {
      console.error('Error creating CRUD item:', err);
      res.status(500).json({ error: 'Failed to create CRUD item' });
    } else {
      console.log('CRUD item created');
      res.status(201).json({ message: 'CRUD item created successfully' });
    }
  });
});


// GET api
router.get("/api/get_event", (req, res) => {
  const sql = "SELECT * FROM crud";

  db.query(sql, (err, results) => {
    if (err) {
      console.error('Error retrieving CRUD items:', err);
      res.status(500).json({ error: 'Failed to retrieve CRUD items' });
    } else {
      console.log('CRUD items retrieved');
      res.status(200).json({ events: results });
    }
  });
});

// PUT api
router.put("/api/update_event/:id", upload.single('image'), (req, res) => {
  if (!req.file) {
    return res.status(400).json({ error: 'No file uploaded' });
  }

  const id = req.params.id;
  const imageFilename = req.file.filename; // Get the uploaded image filename
  const { eevent, etime, elocation } = req.body; // Include other event details in the request body

  const sql = "UPDATE crud SET eevent = ?, etime = ?, elocation = ?, image = ? WHERE id = ?";
  const values = [eevent, etime, elocation, imageFilename, id];

  db.query(sql, values, (err, result) => {
    if (err) {
      console.error('Error updating event:', err);
      res.status(500).json({ error: 'Failed to update event' });
    } else {
      console.log('Event updated');
      res.status(200).json({ message: 'Event updated successfully' });
    }
  });
});


// DELETE api
router.delete("/api/delete/:id", (req, res) => {
  const id = req.params.id;

  const sql = "DELETE FROM crud WHERE id = ?";

  db.query(sql, id, (err, result) => {
    if (err) {
      console.error('Error deleting CRUD item:', err);
      res.status(500).json({ error: 'Failed to delete CRUD item' });
    } else {
      console.log('CRUD item deleted');
      res.status(204).end();
    }
  });
});

module.exports = router;

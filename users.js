const express=require('express');
const router=express.Router();
var db=require('./db.js');

router.route('/register').post((req,res)=>{
    //get parameters
    var firstName=req.body.firstName;
    var lastName=req.body.lastName;
    var email=req.body.email;
    var mobileNumber=req.body.mobileNumber;
    var password=req.body.password;

    //Create query 
    var sqlQuery="INSERT INTO users(firstName, lastName, email, mobileNumber, password) VALUES (?,?,?,?,?)";

    //call databsase to insert so add or include database
    // ???? pass parameters here

    db.query(sqlQuery,[firstName,lastName,email,mobileNumber,password], function(error,data,fields){

        if(error)
            {
                //error response
                res.send(JSON.stringify({success:false,message:error}));
            }else{
                // success response
                res.send(JSON.stringify({success:true,message:'register'}));
            };
    });
});

router.route('/login').post((req,res)=>{

    var email=req.body.email;
    var password=req.body.password;

    var sql="SELECT * FROM users WHERE email=? AND password=?";

    //write again
    db.query(sql,[email,password],function(err,data,fields){
        if(err){
            res.send(JSON.stringify({success:false,message:err}));
        }else{
            if(data.length > 0){
            res.send(JSON.stringify({success:true,message:data}));
        }else{
            res.send(JSON.stringify({success:false,message:'Empty Data'}));
        }
    }
    });
});






module.exports =router;
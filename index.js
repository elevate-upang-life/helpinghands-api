const express = require('express');
const app = express();
const multer = require('multer');
const path = require('path');
var bodyParser = require('body-parser');

const storage = multer.diskStorage({
    destination: './src/image/',
    filename:(req, file, cb) => {
        return cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`)
    }
})

const upload = multer({
    storage: storage
})

app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
const userRouter = require('./users');
app.use('/users', userRouter);
const crudRouter = require('./crud.js');
app.use('/', crudRouter);

// Serve static files from the 'src/image' directory
app.use('/src/image', express.static('src/image'));

const eventData = [];

var db = require('./db.js');

app.listen(3000, console.log("your server start on port 3000"));

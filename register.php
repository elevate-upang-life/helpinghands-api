<?php
$db = mysqli_connect('localhost', 'root', '', 'hhapp');
if (!$db) {
    echo json_encode(array("status" => "Error", "message" => "Database Connection Failed"));
    exit; // Terminate the script
}

$firstName = $_POST["firstName"];
$lastName = $_POST["lastName"];
$email = $_POST["email"];
$mobileNumber = $_POST["mobileNumber"];
$password = $_POST["password"];

// Validate email format
if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    echo json_encode(array("status" => "Error", "message" => "Invalid email format"));
    exit;
}

// Hash the password
$hashedPassword = password_hash($password, PASSWORD_BCRYPT);

// Use prepared statements to prevent SQL injection
$stmt = $db->prepare("SELECT * FROM users WHERE email = ?");
$stmt->bind_param("s", $email);
$stmt->execute();
$result = $stmt->get_result();

if ($result->num_rows == 1) {
    echo json_encode(array("status" => "Error", "message" => "User Already Exists"));
} else {
    // Use prepared statement for INSERT
    $stmt = $db->prepare("INSERT INTO users (firstName, lastName, email, mobileNumber, password) VALUES (?, ?, ?, ?, ?)");
    $stmt->bind_param("sssss", $firstName, $lastName, $email, $mobileNumber, $hashedPassword);

    if ($stmt->execute()) {
        echo json_encode(array("status" => "Success", "message" => "Registration Success"));
    } else {
        echo json_encode(array("status" => "Error", "message" => "Registration Failed"));
    }
}

// Close the database connection
mysqli_close($db);
?>